package main

import "os"
import "fmt"
import "net"
import "encoding/json"

func main() {
    ui := NewChatUI()
    go ui.Start()

    var me string = os.Args[1]

    hide_init()

    listener, err := net.Listen("tcp", "0.0.0.0:1234")
    if err != nil {
        println("error listening:", err)
        os.Exit(1)
    }


    var die_ch = make(chan bool)
    for {
        conn, err := listener.Accept()
        if err != nil {
            println("Error accept:", err)
            return
        }

        dec := json.NewDecoder(conn)
        enc := json.NewEncoder(conn)

        err = enc.Encode(me)
        if err != nil {
            ui.Inbound <- fmt.Sprintf("Connection Died: %v", err)
            continue
        }

        var other string
        err = dec.Decode(&other)
        if err != nil {
            ui.Inbound <- fmt.Sprintf("Connection Died: %v", err)
            continue
        }

        ui.SetOther(other)
        go func() {
            for {
                var message string
                err := dec.Decode(&message)
                if err != nil {
                    ui.Inbound <- fmt.Sprintf("Connection Died: %v", err)
                    die_ch <- true
                    return
                }
                ui.Inbound <- message
            }
        }()

        func () {
            for {
                select {
                case message, ok := <-ui.Outbound:
                    if !ok {
                        return
                    }
                    err := enc.Encode(message)
                    if err != nil {
                        ui.Inbound <- fmt.Sprintf("Connection Died: %v", err)
                    }
                case <-die_ch:
                    return
                }
            }
        }()
    }
}
