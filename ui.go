package main

import "fmt"
import gc "bitbucket.org/ndavies/goncurses"

const INPUT_HEIGHT = 1

type Window struct {
    border bool
    Win gc.Window
    H int
    W int
    X int
    Y int
}

func (w *Window) Refresh() {
    if w.border {
        w.Win.Box(gc.ACS_VLINE, gc.ACS_HLINE)
    } else {
        w.Win.Box(0, 0)
    }
    w.Win.Refresh()
}

type ChatUI struct {
    hidden bool
    other string
    messages []string
    unread UnreadCounter

    stdscr gc.Window
    input_window *Window
    messages_window *Window

    Inbound chan string
    Outbound chan string
}

func NewChatUI() ChatUI {
    return ChatUI{
        other: "unknown",
        messages: make([]string, 0, 100),
        unread: UnreadCounter{},

        Inbound: make(chan string),
        Outbound: make(chan string),
    }
}

func (ui *ChatUI) SetupWindows(first bool) {
    var err error

    if !first {
        destroy_window(ui.input_window)
        destroy_window(ui.messages_window)
    }

    gc.End()
    ui.stdscr.Erase()
    ui.stdscr.Refresh()
    rows, cols := ui.stdscr.Maxyx()

    ui.input_window, err = create_window(INPUT_HEIGHT + 2, cols, rows - INPUT_HEIGHT - 2, 0, true)
    if err != nil {
        panic(err)
    }

    ui.messages_window, err = create_window(rows - INPUT_HEIGHT - 2, cols, 0, 0, false)
    if err != nil {
        panic(err)
    }
    ui.messages_window.Win.ScrollOk(true)
    ui.messages_window.Win.SetScrReg(1, rows - INPUT_HEIGHT - 4)
}

func (ui *ChatUI) Start() {
    ui.stdscr, _ = gc.Init()
    defer gc.End()

    gc.CBreak(true)
    gc.Cursor(0)

    ui.unread.Read()
    ui.SetupWindows(true)
    ui.messages_window.Refresh()
    ui.input_window.Refresh()
    go func() {
        for message := range ui.Inbound {
            ui.unread.Inc()
            ui.AddMessage(ui.other, message, true)
        }
    }()

    for {
        ui.input_window.Win.Move(1, 1)
        message, err := ui.input_window.Win.GetString(200)
        if err != nil {
            panic(err)
        }

        ui.unread.Read()
        if message == "" {
            ui.Refresh()
            continue
        }
        if message[0] == "/"[0] || message[0] == `\`[0] {
            switch message[1:] {
            case "clear":
                ui.messages_window.Win.Clear()
                ui.messages = make([]string, 0, 100)
            case "h", "hide":
                ui.hidden = true
                gc.Echo(false)
            case "uh", "unhide":
                ui.hidden = false
                gc.Echo(true)
            }
        } else {
            if !ui.hidden {
                ui.AddMessage("me", message, false)
                ui.Outbound <- message
            }
        }

        ui.Refresh()
    }

}

func (ui *ChatUI) SetOther(other string) {
    ui.other = other
}

func (ui *ChatUI) AddMessage(from string, m string, draw bool) {
    msg := fmt.Sprintf("%s: %s", from, m)
    ui.messages = append(ui.messages, msg)

    if draw && !ui.hidden {
        ui.messages_window.Win.Scroll(1)
        ui.messages_window.Win.MovePrint(ui.messages_window.H - 2, 1, msg)
        ui.messages_window.Refresh()
    }
}

func (ui *ChatUI) Refresh() {
    if !ui.hidden {
        ui.SetupWindows(false)

        for _, m := range ui.messages {
            ui.messages_window.Win.Scroll(1)
            ui.messages_window.Win.MovePrint(ui.messages_window.H - 2, 1, m)
        }

        ui.messages_window.Refresh()
        ui.input_window.Refresh()
    } else {
        ui.stdscr.Clear()
        ui.stdscr.MovePrint(0, 0, hidden_str)
        ui.stdscr.Refresh()
    }
}

func destroy_window(w *Window) {
    w.Win.Border(' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ')
    w.Win.Erase()
    w.Win.Refresh()
    w.Win.Delete()
    return
}

func create_window(rows, cols, y, x int, border bool) (*Window, error) {
    w, err := gc.NewWindow(rows, cols, y, x)
    if err != nil {
        return nil, err
    }

    return &Window{
        border: border,
        Win: w,
        H: rows,
        W: cols,
        X: x,
        Y: y,
    }, nil
}

