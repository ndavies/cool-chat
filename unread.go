package main

import "fmt"
import "os"

type UnreadCounter struct {
    count int
}

func (ur *UnreadCounter) Write() {
    f, err := os.Create("/tmp/coolchat.unread")
    if err != nil {
        panic(err)
    }

    f.Write([]byte(fmt.Sprint(ur.count)))
}

func (ur *UnreadCounter) Inc(){
    ur.count++
    ur.Write()
}

func (ur *UnreadCounter) Read(){
    ur.count = 0
    ur.Write()
}
